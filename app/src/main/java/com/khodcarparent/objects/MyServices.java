package com.khodcarparent.objects;

/**
 * Created by alireza on 7/9/2017.
 */

public class MyServices {
    String schoolName;

    public MyServices(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }
}
