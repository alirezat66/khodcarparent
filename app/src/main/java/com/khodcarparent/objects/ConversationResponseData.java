package com.khodcarparent.objects;


/**
 * Created by alireza on 3/13/2017.
 */

public class ConversationResponseData  {

    public  String fname;
    public  String lname;
    public  String imgUrl;
    public  String state;
    public String unseen;


    public ConversationResponseData() {
    }

    public ConversationResponseData(String fname, String lname, String imgUrl, String state, String unseen) {
        this.fname = fname;
        this.lname = lname;
        this.imgUrl = imgUrl;
        this.state = state;
        this.unseen = unseen;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getState() {
        return state;
    }

    public String getUnseen() {
        return unseen;
    }
}



