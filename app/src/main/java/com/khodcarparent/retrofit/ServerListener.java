package com.khodcarparent.retrofit;

import com.google.gson.JsonObject;

public interface ServerListener {
    void onFailure(int i, String str);

    void onSuccess(int i, JsonObject jsonObject);
}
