package com.khodcarparent.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.khodcarparent.R;
import com.khodcarparent.adapter.HistoryAdapter;
import com.khodcarparent.objects.PrivateMessageDataBase;

import java.util.ArrayList;

/**
 * Created by alireza on 7/7/2017.
 */

public class ChatingActivity extends AppCompatActivity {
    ListView list ;
    HistoryAdapter adapter;
    EditText chat_edit_text;
    ImageView imgSend;
    ArrayList<PrivateMessageDataBase>messagesList=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_messages);

        super.onCreate(savedInstanceState);

        makePage();
        makeList();
    }

    private void makeList() {
        PrivateMessageDataBase md1  = new PrivateMessageDataBase(1,"me","لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که");
        PrivateMessageDataBase md2  = new PrivateMessageDataBase(2,"you","لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت");
        PrivateMessageDataBase md3  = new PrivateMessageDataBase(3,"you","گفته که لطفا آخرین ورژنش رو نصب کنید دیگه");
        PrivateMessageDataBase md4  = new PrivateMessageDataBase(4,"","");
        PrivateMessageDataBase md5  = new PrivateMessageDataBase(5,"you","والا احتمالا بخوايم بريم گرجستان يا تو مرداد يا شهريور ، شمالم اگه بشه يه سر ميايم ولي ممكنه بره واسه مهر يا آبان");
        PrivateMessageDataBase md6  = new PrivateMessageDataBase(6,"me","هیچی فعلا یه جلسه رفتیم ایشالا تو 1 ماه آینده بقیه جلسات هم می ریم");
        PrivateMessageDataBase md7  = new PrivateMessageDataBase(7,"","");
        PrivateMessageDataBase md8  = new PrivateMessageDataBase(8,"you","اینکه بتونی تشخیص بدی که این اختلاف سلیقه ها چقدر مهمه و یا اختلاف آوره یا نه مهمه . من خودم نمی تونستم تشخیص بدم");
        PrivateMessageDataBase md9  = new PrivateMessageDataBase(9,"","");
        PrivateMessageDataBase md10  = new PrivateMessageDataBase(10,"me","درست، خب  الان مشاوره به كجا رسيد ؟");
        PrivateMessageDataBase md11  = new PrivateMessageDataBase(11,"me","هیچی فعلا یه جلسه رفتیم ایشالا تو 1 ماه آینده بقیه جلسات هم می ریم");

        messagesList.add(md1);
        messagesList.add(md2);
        messagesList.add(md3);
        messagesList.add(md4);
        messagesList.add(md5);
        messagesList.add(md6);
        messagesList.add(md7);
        messagesList.add(md8);
        messagesList.add(md9);
        messagesList.add(md10);
        messagesList.add(md11);


        adapter  = new HistoryAdapter(ChatingActivity.this,messagesList);


        list.setAdapter(adapter);


    }

    private void makePage() {
        list = (ListView) findViewById(R.id.list);

        chat_edit_text = (EditText) findViewById(R.id.chat_edit_text);
        imgSend = (ImageView) findViewById(R.id.send_btn);
        chat_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if(chat_edit_text.getText().length()>0){
                    imgSend.setImageResource(R.drawable.ic_send);
                }else {
                    imgSend.setImageResource(R.drawable.ic_send_enactive);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
