package com.khodcarparent.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.khodcarparent.R;
import com.khodcarparent.helper.permissionClass;

public class SplashActivity extends Activity {
    public static String[] mPermission = new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    Context context;
    int sumNeedPermission = 0;
  //  TextView txtVersionl;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        this.context = this;
     //   this.txtVersionl = (TextView) findViewById(R.id.txtversion);
       /* try {
            this.txtVersionl.setText("نسخه :  " + this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }*/
        checkPermissions();
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (!(VERSION.SDK_INT < 23 || context == null || permissions == null)) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != 0) {
                    this.sumNeedPermission++;
                    return false;
                }
            }
        }
        return true;
    }

    private void checkPermissions() {
        if (hasPermissions(this, mPermission)) {
            WelcomeScreen();
        } else {
            ActivityCompat.requestPermissions(this, mPermission, 58175817);
        }
    }

    public void WelcomeScreen() {
        new Thread() {
            int wait = 0;

            public void run() {
                try {
                    super.run();
                    while (this.wait < 4000) {
                        this.sleep(100);
                        this.wait += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {
                    Intent intent;
                    intent = new Intent(SplashActivity.this, MainActivity.class);

                   /* if (PreferencesData.getString(SplashActivity.this.context, "userid", "").trim().equals("")) {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    } else {
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                    }*/
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
            }
        }.start();
    }

    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != 58175817) {
            return;
        }
        if (permissionClass.checkCourseLocationPermission(this.context) && permissionClass.checkFineLocation(this.context)) {
            WelcomeScreen();
            return;
        }
        Toast.makeText(this.context, "You dont set all of needed permissions, you can close app and start again", Toast.LENGTH_LONG).show();
        Toast.makeText(this.context, "You dont set all of needed permissions, you can close app and start again", Toast.LENGTH_LONG).show();
        Toast.makeText(this.context, "You dont set all of needed permissions, you can close app and start again", Toast.LENGTH_LONG).show();
    }

  /*  private void logUser() {
        Crashlytics.setUserIdentifier("12345");
        Crashlytics.setUserEmail("user@fabric.io");
        Crashlytics.setUserName("Test User");
    }*/
}
