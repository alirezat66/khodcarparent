package com.khodcarparent.dialog;

public interface DialogListener {
    void accepted();

    void rejected();
}
