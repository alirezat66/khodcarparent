package com.khodcarparent.Controller;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.khodcarparent.helper.FontsOverride;
import com.khodcarparent.helper.Statics;

import java.io.File;


public class AppController extends Application {
    private static Activity CurrentActivity = null;
    private static Context CurrentContext;

    public void onCreate() {
        super.onCreate();
/*
        Fabric.with(this, new Crashlytics());
*/
        FontsOverride.setDefaultFont(getApplicationContext(), "MONOSPACE", "IRANSansMobile.ttf");
        File f;
        f = new File(Statics.SHOP_DIRECTORY);
        if (!f.exists()) {
            f.mkdir();
        }

        f = new File(Statics.CACHE_DIRECTORY);
        if (!f.exists()) {
            f.mkdir();
        }
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static void setCurrentContext(Context mCurrentContext) {
        CurrentContext = mCurrentContext;
    }

    public static void setActivityContext(Activity activity, Context context) {
        CurrentActivity = activity;
        CurrentContext = context;
    }

    public static Activity getCurrentActivity() {
        return CurrentActivity;
    }

    public static Context getCurrentContext() {
        return CurrentContext;
    }
}
